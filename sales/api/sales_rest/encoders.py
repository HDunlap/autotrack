from .models import SalesPerson, Customer, SalesRecord, VehicleVO
from common.json import ModelEncoder


class VehicleVOEncoder (ModelEncoder):
    model = VehicleVO
    properties = [
        "import_href",
        "vin",
        "year",
        "manufacturer",
        "model",
    ]


class SalesPersonEncoder (ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_id",
    ]


class CustomerEncoder (ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id"
    ]


class SalesRecordEncoder (ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "price",
        "vehicle",
        "sales_person",
        "customer",
    ]
    encoders = {
        "vehicle": VehicleVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
