import React from 'react';
import logo from '../images/Autotrack-1.png'
import { Link } from 'react-router-dom';

function Footer() {
    const containerStyle = {
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
      };

  return (
    <footer style={{position: 'absolute', bottom: 0, width:'100%', height: '3.5rem'}}>
        <div className="bg-primary" style={{containerStyle, color: 'white', padding: "1%"}}>
            <div className='container'>
                <div className='row text-decoration-none' >
                    <div className='col-2'>
                        <ul className='list-unstyled'>
                            <li><img src={logo} alt="logo" style={{width: '100%'}}/></li>
                            <li className='' style={{paddingLeft: '5%'}}>The premiere solution for dealership management.</li>
                        </ul>
                    </div>
                    <div className='col' style={{paddingLeft: '10%'}}>
                        <ul className='list-unstyled'>
                            <li>
                                <h5>Links</h5>
                            </li>
                            <li>
                                <Link to="/" className="text-light" >Home</Link>
                            </li>
                            <li>
                                <Link to="/inventory" className="text-light" >Inventory</Link>
                            </li>
                            <li>
                                <Link to="/records" className="text-light" >Sales</Link>
                            </li>
                            <li>
                                <Link to="/service" className="text-light" >Service</Link>
                            </li>

                        </ul>
                    </div>
                    <div className='col'>
                        <ul className='list-unstyled'>
                            <li>
                                <h5>Contact us</h5>
                            </li>
                            <li>
                                (800) 123-0000
                            </li>
                            <li>
                                Autotrack@example.com
                            </li>
                            <li>
                                123 Street St, Town, State 12345
                            </li>
                        </ul>
                    </div>
                    <div className='col'>
                        <ul className='list-unstyled'>
                            <h5>Support</h5>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  );
}

export default Footer;
