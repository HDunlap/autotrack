import { Link, NavLink } from 'react-router-dom';
import logo from '../images/Autotrack-1.png'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/" style={{width: '14%'}}>
          <img src={logo} alt="logo" style={{width: '100%'}}/>
        </NavLink>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mb-2 mb-lg-0" style={{margin: 0}}>

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li >
                <Link className="dropdown-item" to="/inventory/">View inventory</Link>
                <hr className="dropdown-divider" style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/inventory/new/">
                  Add to inventory
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <Link className="dropdown-item" to="/records/new/">Record a sale</Link>
                <hr className="dropdown-divider" style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/records/">All sales</Link>
                <hr className="dropdown-divider" style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/records/employee/">Sales by employee</Link>
              </li>
            </ul>
          </li>

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Service
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li >
                <Link className="dropdown-item" to="/service/schedule/">Schedule appointment</Link>
                <hr className="dropdown-divider" style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/service/">Upcoming appointments</Link>
                <hr className="dropdown-divider" style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/service/history/">Service history</Link>
              </li>
            </ul>
          </li>

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Employees
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li >
                <Link className="dropdown-item" to="/employees/sales/new/">Add sales person</Link>
                <hr style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/employees/service/new/">Add technician</Link>
                <hr style={{margin: 5}} />
              </li>
              <li >
                <Link className="dropdown-item" to="/employees/">Employee list</Link>
              </li>
            </ul>
          </li>



            <li >
              <Link className="nav-link" to="/customers/new/">Add customer</Link>
            </li>


          </ul>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="/navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
