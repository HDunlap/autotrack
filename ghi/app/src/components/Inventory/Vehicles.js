import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';


function ListVehicles() {
    const [vehicles, setVehicles] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");

        if (response.ok) {
            const data = await response.json();
            setVehicles(data.autos)
        }
    }

    const formatPrice = (num) => {
        return num.toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD'
        })
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <>
        <div className="container" style={{padding: '3em'}}>
            <div>
                <div className='row' style={{width:"100%"}}>
                    <div style={{paddingLeft:10}} className='col-10'>
                        <h1>Inventory</h1>
                    </div>
                    <div className='col-2 justify-content-end'>
                        <Link to="/inventory/new">
                            <button  className='btn btn-primary justify-content-end mt-3'>Add vehicle</button>
                        </Link>
                    </div>
                </div>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Price</th>
                        <th>Mileage</th>
                        <th>VIN</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicles.map(vehicle => {
                        return (
                            <tr key={vehicle.vin}>
                                <td>{vehicle.year}</td>
                                <td>{vehicle.model.manufacturer.name}</td>
                                <td>{vehicle.model.name}</td>
                                <td>{formatPrice(vehicle.price)}</td>
                                <td>{vehicle.mileage.toLocaleString()}</td>
                                <td>{vehicle.vin}</td>
                                <td>{vehicle.status.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default ListVehicles;
