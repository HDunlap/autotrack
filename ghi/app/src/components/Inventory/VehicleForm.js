import { useState } from "react";


function VehicleForm() {
    const [vinNumber, setVinNumber] = useState("")

    const [formData, setFormData] = useState({
      year: "",
      make: "",
      model: "",
      vin: "",
      trim: "",
      color: "",
      price: "",
      mileage: "",
    })

    const parseNum = (num) => {
      return num.replace(/[^.\d]/g, '')
    }

    const handleVinSearch = async (e) => {
      const vin = vinNumber
      const apiUrl = `https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVinValues/${vin}?format=json`

      const response = await fetch(apiUrl)

        if (response.ok) {
            const data = await response.json();
            setFormData({
              ...formData,
              vin: vin,
              year: data.Results[0]["ModelYear"],
              make: data.Results[0]["Make"],
              model: data.Results[0]["Model"],
            });
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            year: formData.year,
            make: formData.make,
            model: formData.model,
            trim: formData.trim,
            vin: formData.vin,
            price: parseNum(formData.price),
            mileage: parseNum(formData.mileage),
            color: formData.color
        }


        const locationUrl = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
              vin: "",
              trim: "",
              color: "",
              price: "",
              mileage: "",
              year: "",
              make: "",
              model: "",
            })
            setVinNumber("")
        }
    }

    const handleFieldChange = (e) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value
      })
    }

    const handleVinChange = (e) => {
      setVinNumber(e.target.value)
      setFormData({
        ...formData,
        year: "",
        make: "",
        model: "",
      })
    }

    return (

        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a vehicle to inventory</h1>
              <div className='input-group mb-3 mt-3'>
                <input onChange={handleVinChange} value={vinNumber} type="text" className='form-control' placeholder='VIN' aria-describedby='basic-addon2' name="vin"/>
                <div className='input-group-append'>
                    <button onClick={handleVinSearch} className='btn btn-outline-secondary' type='button'>Search VIN</button>
                </div>
              </div>
              <form onSubmit={handleSubmit} id="add-vehicle-form">
                <table style={{width: "100%"}}>
                  <tbody>
                    <tr>
                      <td>
                        <div className="form-floating mb-3">
                          <input readOnly value={formData.year} disabled placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                          <label htmlFor="year">Year</label>
                        </div>
                      </td>
                      <td>
                        <div className="form-floating mb-3">
                          <input readOnly value={formData.make} disabled placeholder="Make" required type="text" name="make" id="make" className="form-control" />
                          <label htmlFor="make">Make</label>
                        </div>
                      </td>
                      <td>
                        <div className="form-floating mb-3">
                          <input readOnly value={formData.model} disabled placeholder="Model" required type="text" name="model" id="model" className="form-control" />
                          <label htmlFor="model">Model</label>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <div className="form-floating mb-3">
                  <input onChange={handleFieldChange} value={formData.mileage} placeholder="Mileage" required type="text" name="mileage" id="mileage" className="form-control" />
                  <label htmlFor="mileage">Mileage</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFieldChange} value={formData.trim} placeholder="Trim" type="text" name="trim" id="trim" className="form-control" />
                  <label htmlFor="trim">Trim (Optional)</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFieldChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFieldChange} value={formData.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                  <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Add vehicle</button>
              </form>
            </div>
          </div>
        </div>
        );


}

export default VehicleForm;
