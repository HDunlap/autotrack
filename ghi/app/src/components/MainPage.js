import background from '../images/pexels-pixabay-63294.jpg'
import { Link } from 'react-router-dom';
import SalesLogo from '../images/SalesLogo.png'
import ServiceLogo from '../images/ServiceLogo.png'
import InventoryLogo from '../images/InventoryLogo.png'

function MainPage() {
  return (
    <div  style={{backgroundImage: `url(${background})`, backgroundSize: "cover", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
      <div style={{backgroundColor: 'rgb(255, 255, 255, .78)' ,backgroundSize: "100% 100%", height: '100vh', width: '100%', paddingRight: 0, paddingLeft: 0, margin: 0, marginRight: 0, marginTop: 0}}>
        <div className="px-4 py-5 text-center">
          <h1 className="display-5 fw-bold" style={{paddingTop: '5%'}}>Welcome to Autotrack!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The premiere solution for dealership
              management.
            </p>
          </div>
          <div className="mt-5">
            <h2>Getting Started</h2>
            <div className="container mt-4">
              <div className="row justify-content-center">

                <div className="col-2">
                  <Link to="/inventory">
                    <button className="btn btn-primary btn-lg" style={{width: '100%'}}>
                      <ul className="list-unstyled">
                        <li>
                        <img src={InventoryLogo} alt="logo" style={{height: '5em'}}/>
                        </li>
                        <li>
                          <h5>Inventory</h5>
                        </li>
                      </ul>
                      </button>
                  </Link>
                </div>

                <div className="col-2">
                  <Link to="/records">
                    <button className="btn btn-primary btn-lg" style={{width: '100%'}}>
                      <ul className="list-unstyled">
                        <li>
                        <img src={SalesLogo} alt="logo" style={{height: '5em'}}/>
                        </li>
                        <li>
                          <h5>Sales</h5>
                        </li>
                      </ul>
                      </button>
                  </Link>
                </div>

                <div className="col-2">
                  <Link to="/service">
                    <button className="btn btn-primary btn-lg" style={{width: '100%'}}>
                      <ul className="list-unstyled">
                        <li>
                        <img src={ServiceLogo} alt="logo" style={{height: '5em'}}/>
                        </li>
                        <li>
                          <h5>Service</h5>
                        </li>
                      </ul>
                      </button>
                  </Link>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
