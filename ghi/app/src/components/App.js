import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AppointmentForm from './service/AppointmentForm';
import ListAppointments from './service/ListAppointments';
import ListEmployees from './employees/ListEmployees';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './employees/SalesPersonForm';
import CustomerForm from './customers/CustomerForm';
import SalesRecordForm from './sales/SalesRecordForm';
import TechnicianForm from './employees/TechnicianForm';
import ServiceHistory from './service/ServiceHistory';
import ListSalesRecords from './sales/ListSalesRecords';
import ListPersonsSales from './sales/ListPersonsSales';
import ListVehicles from './Inventory/Vehicles';
import VehicleForm from './Inventory/VehicleForm';
import RescheduleForm from './service/RescheduleAppointment';
import Footer from './Footer';

function App() {
  return (
    <BrowserRouter>
      <div style={{position: 'relative', minHeight: '100vh'}}>
        <Nav />
        <div style={{paddingBottom:'3.5rem'}}>
            <Routes>

              <Route path="/" element={<MainPage />} />

              <Route path="employees">
                <Route index element={<ListEmployees />} />
                <Route path="sales/new/" element={<SalesPersonForm />} />
                <Route path='service/new/' element={<TechnicianForm />} />
              </Route>

              <Route path="records">
                <Route index element={<ListSalesRecords />} />
                <Route path="new/" element={<SalesRecordForm />} />
                <Route path="employee/" element={<ListPersonsSales />} />
              </Route>

              <Route path="/customers/new/" element={<CustomerForm />} />

              <Route path='service'>
                <Route index element={<ListAppointments />} />
                <Route path='schedule' element={<AppointmentForm />} />
                <Route path='history' element={<ServiceHistory />} />
                <Route path='reschedule' element={<RescheduleForm />} />
              </Route>

              <Route path='inventory'>
                <Route index element={<ListVehicles />} />
                <Route path='new' element={<VehicleForm />} />
              </Route>

            </Routes>
        </div>
        <Footer/>
      </div>
    </BrowserRouter>
  );
}

export default App;
