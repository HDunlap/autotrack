from common.json import ModelEncoder

from .models import Vehicle, Manufacturer, VehicleModel, Status


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
    ]


class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "id",
        "name",
    ]


class VehicleModelEncoder(ModelEncoder):
    model = VehicleModel
    properties = [
        "id",
        "name",
        "manufacturer",
    ]
    encoders = {
        "manufacturer": ManufacturerEncoder(),
    }


class VehicleEncoder(ModelEncoder):
    model = Vehicle
    properties = [
        "vin",
        "year",
        "model",
        "status",
        "trim",
        "color",
        "price",
        "mileage",
    ]
    encoders = {
        "model": VehicleModelEncoder(),
        "status": StatusEncoder(),
    }
