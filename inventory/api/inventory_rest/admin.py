from django.contrib import admin
from .models import Manufacturer, VehicleModel, Vehicle, Status


admin.site.register(Vehicle)
admin.site.register(Manufacturer)
admin.site.register(VehicleModel)
admin.site.register(Status)
