from django.db import models
from django.urls import reverse


class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})

    class Meta:
        ordering = ("name",)


class VehicleModel(models.Model):
    name = models.CharField(max_length=100)

    manufacturer = models.ForeignKey(
        Manufacturer,
        related_name="models",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_vehicle_model", kwargs={"pk": self.id})

    class Meta:
        ordering = ("name",)


class Status(models.Model):
    name = models.CharField(max_length=20, unique=True)


class Vehicle(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    year = models.PositiveSmallIntegerField()
    trim = models.CharField(max_length=150, null=True)
    mileage = models.PositiveIntegerField()
    color = models.CharField(max_length=50)
    price = models.PositiveIntegerField()

    model = models.ForeignKey(
        VehicleModel,
        related_name="vehicles",
        on_delete=models.CASCADE,
    )

    status = models.ForeignKey(
        Status,
        related_name="vehicles",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_vehicle", kwargs={"vin": self.vin})
