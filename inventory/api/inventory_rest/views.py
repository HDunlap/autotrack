from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    VehicleEncoder,
    ManufacturerEncoder,
    VehicleModelEncoder,
)
from .models import Vehicle, Manufacturer, VehicleModel, Status


@require_http_methods(["GET", "POST"])
def api_vehicles(request):
    if request.method == "GET":
        autos = Vehicle.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=VehicleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            make = Manufacturer.objects.get_or_create(name=content["make"])
            del content["make"]
            model = VehicleModel.objects.get_or_create(name=content["model"], manufacturer=make[0])
            content["model"] = model[0]
            status = Status.objects.get(name="Available")
            content["status"] = status
            auto = Vehicle.objects.create(**content)
            return JsonResponse(
                auto,
                encoder=VehicleEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add the vehicle"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_vehicle(request, vin):
    if request.method == "GET":
        try:
            auto = Vehicle.objects.get(vin=vin)
            return JsonResponse(
                auto,
                encoder=VehicleEncoder,
                safe=False
            )
        except Vehicle.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Vehicle.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=VehicleEncoder,
                safe=False,
            )
        except Vehicle.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            auto = Vehicle.objects.get(vin=vin)

            props = ["color", "year", "status"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=VehicleEncoder,
                safe=False,
            )
        except Vehicle.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_manufacturers(request):
    if request.method == "GET":
        manufacturers = Manufacturer.objects.all()
        return JsonResponse(
            {"manufacturers": manufacturers},
            encoder=ManufacturerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.create(**content)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the manufacturer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_manufacturer(request, pk):
    if request.method == "GET":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            manufacturer.delete()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(manufacturer, prop, content[prop])
            manufacturer.save()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_vehicle_models(request):
    if request.method == "GET":
        models = VehicleModel.objects.all()
        return JsonResponse(
            {"models": models},
            encoder=VehicleModelEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer_id = content["manufacturer_id"]
            manufacturer = Manufacturer.objects.get(id=manufacturer_id)
            content["manufacturer"] = manufacturer
            model = VehicleModel.objects.create(**content)
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the vehicle model"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_vehicle_model(request, pk):
    if request.method == "GET":
        try:
            model = VehicleModel.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False
            )
        except VehicleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = VehicleModel.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        except VehicleModel.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            model = VehicleModel.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        except VehicleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
